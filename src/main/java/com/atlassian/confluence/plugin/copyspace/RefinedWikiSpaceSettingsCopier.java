package com.atlassian.confluence.plugin.copyspace;

import com.atlassian.confluence.spaces.Space;

public interface RefinedWikiSpaceSettingsCopier {

    void copySpaceLayoutSetting(Space source, Space destination);
}
