package com.atlassian.confluence.plugin.copyspace;

import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.spaces.Space;
import org.apache.log4j.Logger;

public class DefaultRefinedWikiSpaceSettingsCopier implements RefinedWikiSpaceSettingsCopier {

    public static final String REFINEDWIKI_ORIGINALTHEME_SPACES_LAYOUT = "refinedwiki.originaltheme.spaces.layout";
    private Logger log = Logger.getLogger(DefaultRefinedWikiSpaceSettingsCopier.class);

    private ContentPropertyManager contentPropertyManager;

    public DefaultRefinedWikiSpaceSettingsCopier(ContentPropertyManager contentPropertyManager) {
        this.contentPropertyManager = contentPropertyManager;
    }

    @Override
    public void copySpaceLayoutSetting(Space source, Space destination) {
        String layoutSettings = contentPropertyManager.getTextProperty(source.getDescription(), REFINEDWIKI_ORIGINALTHEME_SPACES_LAYOUT);
        if (layoutSettings != null && !layoutSettings.equals("")) {
            log.info("Copying RefinedWiki Original Theme Space Layout data..");
            contentPropertyManager.setTextProperty(destination.getDescription(), REFINEDWIKI_ORIGINALTHEME_SPACES_LAYOUT,layoutSettings);
            log.info("Copying RefinedWiki Original Theme Space Layout done.");
        } else {
            log.info("No RefinedWiki Original Theme Space Layout data found - skipping.");
        }
    }
}
